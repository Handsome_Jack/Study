#include <cmath>
#include <iostream>
#include <SDL2/SDL.h>
#define Pi 3.14159265358979323846
struct Mat
{
	float array[4][4];
};

float arr[12][3] = {{-1,-1,1},
                    {1,-1,1},
                    {-1,1,1},
                    {1,1,1},
                    {0,-0.84,1.4},
                    {0,0.84,1.4}
                    };

int rebra[18][2] = {{0,4}, {1,4}, {2,5}, {3,5}, {4,5}};

Mat Identity()
{
    Mat m;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			m.array[i][j] = i == j;
	return m;
}

void Product(Mat a, float arr[3],float result[4])
{
    float arr_1[4];
    std::copy(arr, arr+3, arr_1);
    arr_1[3] = 1;
    for(int y = 0; y <4; y++)
    {
        result[y] = 0;
        for(int i = 0; i < 4; i++)
            result[y] += a.array[i][y]*arr_1[i];
    }
}

Mat operator*(Mat a, Mat b)
{
	Mat mat;
	for(int y = 0; y < 4; y++)
		for (int x = 0; x < 4; x++)
		{
			mat.array[x][y] = 0;
			for (int i = 0; i < 4; i++)
				mat.array[x][y] += a.array[i][y] * b.array[x][i];
		}
	return mat;
}

Mat Translate(float x, float y, float z)
{
	Mat m = Identity();
	m.array[3][0] = x;
	m.array[3][1] = y;
	m.array[3][2] = z;
	return m;
}

Mat Rotate(float angle, float x, float y, float z)
{
	Mat m;
	angle *= M_PI / 180;
	float len = std::sqrt(x*x + y*y + z*z);
	x /= len;
	y /= len;
	z /= len;
	float c = std::cos(angle), s = std::sin(angle);
	m.array[0][0] = x * x * (1 - c) + c; m.array[1][0] = x * y * (1 - c) - z * s; m.array[2][0] = x * z * (1 - c) + y * s; m.array[3][0] = 0;
	m.array[0][1] = y * x * (1 - c) + z * s; m.array[1][1] = y * y * (1 - c) + c; m.array[2][1] = y * z * (1 - c) - x * s; m.array[3][1] = 0;
	m.array[0][2] = x * z * (1 - c) - y * s; m.array[1][2] = y * z * (1 - c) + x * s; m.array[2][2] = z * z * (1 - c) + c; m.array[3][2] = 0;
	m.array[0][3] = 0; m.array[1][3] = 0; m.array[2][3] = 0; m.array[3][3] = 1;
	return m;
}

Mat Scale(float x, float y, float z)
{
	Mat m = Identity();
	m.array[0][0] = x;
	m.array[1][1] = y;
	m.array[2][2] = z;
	return m;
}

int main(int, char**)
{
    if (SDL_Init(SDL_INIT_VIDEO) == 0) {
        SDL_Window* window = NULL;
        SDL_Renderer* renderer = NULL;
        if (SDL_CreateWindowAndRenderer(640, 480, 0, &window, &renderer) == 0) {
           bool done = false;
           Mat m = Translate(320,240,0), m2 = Scale(50,50,50);
           m = Rotate(25,1,-1,0)*m;
           float ma1[4],ma2[4],ma3[4],ma4[4];
           float q1[3]={0,0,0},q2[3]={-3,0,0},q3[3]={0,-3,0},q4[3]={0,0,-3};
           Product(m*m2,q1,ma1);
           Product(m*m2,q2,ma2);
           Product(m*m2,q3,ma3);
           Product(m*m2,q4,ma4);

            while (!done) {
                SDL_Event event;

                SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

                SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
                Mat mt[6] = {Identity(), Scale(-1,-1,-1), Mat{0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,1},
                Mat{0,-1,0,0,0,0,-1,0,-1,0,0,0,0,0,0,1}, Mat{0,0,1,0,1,0,0,0,0,1,0,0,0,0,0,1},
                Mat{0,0,-1,0,-1,0,0,0,0,-1,0,0,0,0,0,1}};
                for(int j = 0; j <6; j++)
                {
                    for(int i =0; i <5; i++)
                    {
                        float mas1[4], mas2[4];
                        Product(m*m2*mt[j], arr[rebra[i][0]],mas1);
                        Product(m*m2*mt[j], arr[rebra[i][1]],mas2);
                        SDL_RenderDrawLine(renderer, mas1[0], mas1[1], mas2[0], mas2[1]);
                    }
                }
                SDL_RenderDrawLine(renderer, ma1[0], ma1[1], ma2[0], ma2[1]);
                SDL_RenderDrawLine(renderer, ma1[0], ma1[1], ma3[0], ma3[1]);
                SDL_RenderDrawLine(renderer, ma1[0], ma1[1], ma4[0], ma4[1]);

                SDL_RenderPresent(renderer);
                while (SDL_PollEvent(&event)) {
                    if (event.type == SDL_QUIT) {
                        done = true;
                    }
                    else if (event.type == SDL_KEYDOWN)
                    {
                        switch (event.key.keysym.scancode)
                        {
                            case SDL_SCANCODE_RIGHT:
                                 m2 = Translate(2,0,0)*m2;  break;
                            case SDL_SCANCODE_LEFT:
                                 m2 = Translate(-2,0,0)*m2; break;
                            case SDL_SCANCODE_UP:
                                 m2 = Translate(0,2,0)*m2; break;
                            case SDL_SCANCODE_DOWN:
                                 m2 = Translate(0,-2,0)*m2; break;
                            case SDL_SCANCODE_M:
                                 m2 = Translate(0,0,2)*m2; break;
                            case SDL_SCANCODE_N:
                                 m2 = Translate(0,0,-2)*m2; break;
                            case SDL_SCANCODE_D:
                                 m2 = Rotate(5,0.5,0,0)*m2; break;
                            case SDL_SCANCODE_A:
                                 m2 = Rotate(5,-0.5,0,0)*m2; break;
                            case SDL_SCANCODE_W:
                                 m2 = Rotate(5,0,0.5,0)*m2; break;
                            case SDL_SCANCODE_S:
                                 m2 = Rotate(5,0,-0.5,0)*m2; break;
                            case SDL_SCANCODE_Z:
                                 m2 = Rotate(5,0,0,0.5)*m2; break;
                            case SDL_SCANCODE_C:
                                 m2 = Rotate(5,0,0,-0.5)*m2; break;
                            case SDL_SCANCODE_P:
                                 m2 = Scale(0.9, 0.9,0.9)*m2; break;
                            case SDL_SCANCODE_L:
                                 m2 = Scale(1.1,1.1,1.1)*m2; break;
                            default: break;
                        }
                    }
                }
            }
        }
            if (renderer) {
                SDL_DestroyRenderer(renderer);
            }
            if (window) {
                SDL_DestroyWindow(window);
            }
        }
    SDL_Quit();
    return 0;
}

