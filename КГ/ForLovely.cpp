#include <cmath>
#include <iostream>
#include <SDL2/SDL.h>
#include <cmath>
#define Pi 3.14159265358979323846
SDL_Renderer* renderer = NULL;
class cristall_1
{
    private:
        int X,Y,Color1, Color2, Color3;
        double R,height, Fi, pitch;
    public:
        cristall_1(int X, int Y, int Color1, int Color2, int Color3, double R, double height, double Fi, double pitch);
        void PutX(int X) {this -> X=X;}
        void PutY(int Y) {this -> Y=Y;}
        int GetX() {return X;}
        int GetY() {return Y;}
        void DrawCristall(SDL_Renderer* renderer, int Color1, int Color2, int Color3);
        void ExpandCristall(double);
        void MoveCristal(int DX, int DY);
};

cristall_1::cristall_1(int X, int Y,int Color1, int Color2, int Color3, double R, double height, double Fi, double pitch)
{
    this -> X = X;
    this -> Y = Y;
    this -> R = R;
    this -> Color1 = Color1;
    this -> Color2 = Color2;
    this -> Color3 = Color3;
    this -> height = height;
    this -> Fi = Fi;
    this -> pitch = pitch;
}

void cristall_1::DrawCristall(SDL_Renderer* renderer, int Color1, int Color2, int Color3)
{
    int prev_x, prev_y;
    for (int i = 0; i <= 6; i++)
    {
        double a = i / 3. * Pi + Fi;
        double l = (i % 2 ? R : R * 0.75);
        int cur_x = X + cos(a) * l,
            cur_y = Y + sin(a) * l * sin(pitch);
        if (i != 0)
        {
            double h = cos(pitch) * height / 2;
            SDL_SetRenderDrawColor(renderer, Color1, Color2, Color3, 255);
            SDL_RenderDrawLine(renderer, cur_x, cur_y-h, prev_x, prev_y-h);
            SDL_RenderDrawLine(renderer, cur_x, cur_y-h, cur_x, cur_y+h);
            SDL_RenderDrawLine(renderer, cur_x, cur_y+h, prev_x, prev_y+h);
        }
        prev_x = cur_x;
        prev_y = cur_y;
    }
}

void cristall_1::ExpandCristall(double Ang)
{
    DrawCristall(renderer, 0, 0, 0);
    R += 5*(Ang);
    height += 10*(Ang);
    DrawCristall(renderer, Color1, Color2, Color3);
}

void cristall_1::MoveCristal(int DX, int DY)
{
    DrawCristall(renderer,0,0,0);
    PutX(X+DX);
    PutY(Y+DY);
    DrawCristall(renderer, Color1, Color2, Color3);
}

int main(int, char**)
{
    if (SDL_Init(SDL_INIT_VIDEO) == 0) {
        SDL_Window* window = NULL;

        if (SDL_CreateWindowAndRenderer(640, 480, 0, &window, &renderer) == 0) {
           bool done = false;

           double a = 0, b = 0;
           double Ang = 0;
           int DX = 0, DY = 0;
            while (!done) {
                SDL_Event event;

                SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
                SDL_RenderClear(renderer);

                SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);

                cristall_1 C1(250,250,255,255,255, 50,200,a,b);

                C1.DrawCristall(renderer, 255,255,255);
                C1.ExpandCristall(Ang);
                C1.MoveCristal(DX, DY);
                SDL_RenderPresent(renderer);
                while (SDL_PollEvent(&event)) {
                    if (event.type == SDL_QUIT) {
                        done = true;
                    }
                    else if (event.type == SDL_KEYDOWN)
                    {
                        switch (event.key.keysym.scancode)
                        {
                            case SDL_SCANCODE_RIGHT:
                                 a += 0.05;  break;
                            case SDL_SCANCODE_LEFT:
                                 a -= 0.05; break;
                            case SDL_SCANCODE_UP:
                                 b += 0.05; break;
                            case SDL_SCANCODE_DOWN:
                                 b -= 0.05; break;
                            case SDL_SCANCODE_C:
                                 Ang += 0.1;  break;
                            case SDL_SCANCODE_Z:
                                 Ang -= 0.1; break;
                            case SDL_SCANCODE_D:
                                 DX += 5; break;
                            case SDL_SCANCODE_A:
                                 DX -= 5; break;
                            case SDL_SCANCODE_W:
                                 DY -= 5; break;
                            case SDL_SCANCODE_S:
                                 DY += 5; break;
                            default: break;
                        }
                    }
                }
            }
        }

        if (renderer) {
            SDL_DestroyRenderer(renderer);
        }
        if (window) {
            SDL_DestroyWindow(window);
        }
    }
    SDL_Quit();
    return 0;
}


