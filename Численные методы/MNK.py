import matplotlib.pyplot as plt
import numpy as np

x = [-1, 0, 1,2,3,4]
y = [0.86603, 1, 0.86603, 0.5, 0, -0.5]
sum_x = 0;
sum_x2 = 0;
sum_y = 0;
sum_xy = 0;
sum_x3 = 0;
sum_x4 = 0;
sum_x2y = 0;

def First(sum_x,sum_y,sum_xy,sum_x2):
	a = (5*sum_xy - sum_x*sum_y)/(5*sum_x2 - (sum_x)**2);
	b = (sum_y - a*sum_x)/5
	k = [a,b];
	return k;

def Second(sum_x,sum_y,sum_xy,sum_x2,sum_x3,sum_x4,sum_x2y):
	# Решение системы 3 на 3 методом Крамера, 
	# Вычисление определиетелей с помощью дополнительных миноров
	# Разложение всегда по первой строке
	m1 = sum_x2*5 - sum_x *sum_x;
	m2 = sum_x3*5 - sum_x*sum_x2;
	m3 = sum_x3*sum_x - sum_x2*sum_x2;
	det = sum_x4*m1 - sum_x3*m2 + sum_x2*m3;
	# Замена первого столбца
	m1_1 = sum_x2*5 - sum_x *sum_x;
	m2_1 = sum_xy*5 - sum_x*sum_y;
	m3_1 = sum_xy*sum_x - sum_x2*sum_y;
	det1 = sum_x2y*m1_1 - sum_x3*m2_1 + sum_x2*m3_1;
	a = det1/det;
	# Замена второго столбца
	m1_2 = sum_xy*5 - sum_x*sum_y;
	m2_2 = sum_x3*5 - sum_x*sum_x2;
	m3_2 = sum_x3*sum_y - sum_xy*sum_x2;
	det2 = sum_x4*m1_2 - sum_x2y*m2_2 + sum_x2*m3_2;
	b = det2/det;
	# Замена третьего столбца
	m1_3 = sum_x2*sum_y - sum_xy*sum_x;
	m2_3 = sum_x3*sum_y - sum_xy*sum_x2;
	m3_3 = sum_x3*sum_x - sum_x2*sum_x2;
	det3 = sum_x4*m1_3 - sum_x3*m2_3 + sum_x2y*m3_3;
	c = det3/det;
	k = [a,b,c];
	return k;

if __name__ == "__main__":
	for i in x:
		sum_x += x[i];
		sum_x2 += x[i]**2;
		sum_y += y[i];
		sum_xy += x[i]*y[i];
		sum_x3 += x[i]**3;
		sum_x4 += x[i]**4;
		sum_x2y += (x[i]**2)*y[i];

	#print("sum_x = {}".format(sum_x))
	#print("sum_x2 = {}".format(sum_x2))
	#print("sum_x3 = {}".format(sum_x3))
	#print("sum_x4 = {}".format(sum_x4))
	#print("sum_xy = {}".format(sum_xy))
	#print("sum_x2y = {}".format(sum_x2y))
	#print("sum_y = {}".format(sum_y))

	k = First(sum_x,sum_y,sum_xy,sum_x2);
	a = k[0];
	b = k[1];
	k2 = Second(sum_x,sum_y,sum_xy,sum_x2,sum_x3,sum_x4,sum_x2y);
	c = k2[0];
	d = k2[1];
	e = k2[2];
	print("Приближающий многочлен первой степени: y = {}*x + {}".format(a,b));
	print("Приближающий многочлен второй степени: y = {}*x^2 + {}*x + {}".format(c,d,e));
	er1 = 0;
	er2 = 0;
	for i in x:
		er1 += (y[i] - (a*x[i] + b))**2;
		er2 += (y[i] - (c*(x[i])**2 + d*x[i] + e))**2;
	print("Сумма квадратов ошибок для многочлена первой стпени: о = {}".format(er1))
	print("Сумма квадратов ошибок для многочлена второй стпени: о = {}".format(er2))
	data = [(x[0],x[1]), (y[0], y[1]), 'r',
			(x[1],x[2]), (y[1], y[2]), 'r',
			(x[2],x[3]), (y[2], y[3]), 'r',
			(x[3],x[4]), (y[3], y[4]), 'r',
			(x[4],x[5]), (y[4], y[5]), 'r']
	plt.plot(*data)
	k = np.arange(-1, 4, 0.1);
	plt.plot(k, a*k + b)
	plt.plot(k, c*k**2 + d*k + e)
	#plt.plot(k, -0.10716927*k**2 - 0.087026704*k + 1.36750954)
	plt.axis([-1,4,-1,2])
	plt.show()