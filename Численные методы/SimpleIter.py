import math
import matplotlib.pyplot as plt
import numpy as np
esp = 0.001

def y_k(x, y):
    y = 1 + math.cos(x)
    return y

def x_k(x, y):
    x = math.log(y+1,10) + 1
    return x

def simple_iter_left_x_y(prev_x, prev_y):
    y_left = y_k(prev_x, prev_y)
    x_left = x_k(prev_x, prev_y)

    e = min(abs(x_left- prev_x), abs(y_left - prev_y))
    if e > esp:
        print("x = ", x_left, "  :  ", "y = ", y_left)
        simple_iter_left_x_y(x_left, y_left)

simple_iter_left_x_y(1, 2)
