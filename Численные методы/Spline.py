from math import pi
import matplotlib.pyplot as plt
import numpy as np
import sympy as sp

def spline(a,b,c,d,x,i):
	return a + b*(x - i) + c*(x - i)**2 + d*(x - i)**3;
if __name__ == "__main__":
	a = [ 1, 0.86603, 0.5, 0]
	b = [-0.0810187, -0.2398727, -0.4594907, -0.5202547]
	c = [0, -0.158854, -0.060764, 0]
	d = [-0.0529512, 0.3269667, 0.0202546, 0]
	S = [0,0,0,0]
	x = np.arange(-5, 5, 0.1)
	data = [(0,1), 
	(spline(a[0], b[0], c[0], d[0], 0,1),
		spline(a[0], b[0], c[0], d[0], 1,1)), 'r',
	(1,2),
	(spline(a[1], b[1], c[1], d[1], 1,2),
		spline(a[1], b[1], c[1], d[1], 2,2)), 'g',
	(2, 3), 
	(spline(a[2], b[2], c[2], d[2], 2,3),
	 spline(a[2], b[2], c[2], d[2], 3,3)), 'b',
	(3, 4),
	(spline(a[3], b[3], c[3], d[3], 3,4),
	 spline(a[3], b[3], c[3], d[3], 4,4)), 'r']
	plt.plot(*data)
	#plt.plot(x, spline(a[0], b[0], c[0], d[0], x,1), label = "Сплайн на участке [0,1]")
	#plt.plot(x, spline(a[1], b[1], c[1], d[1], x,2), label = "Сплайн на участке [1,2]")
	#plt.plot(x, spline(a[2], b[2], c[2], d[2], x,3), label = "Сплайн на участке [2,3]")
	#plt.plot(x, spline(a[3], b[3], c[3], d[3], x,4),  label = "Сплайн на участке [3,4]")
	x = 1.5
	y = spline(a[1], b[1], c[1], d[1], 1.5 ,2)
	print("Значение точки функции в точке 1.5 : " + str(y))
	plt.plot(x,y, "ro")
	plt.axis([0,4,-5,5])
	plt.show()