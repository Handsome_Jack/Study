# coding=<utf-8>
from math import cos
from math import pi
import matplotlib.pyplot as plt
import numpy as np
import sympy as sp

arr1 = [0, pi/6, 2*pi/6, 3*pi/6]
arr2 = [0, pi/6, 5*pi/12, pi/2]

def y_res(x):
	return cos(x);

def basis_poly_0(arr, x):
	p_0 = (((x-arr[1])/(arr[0]-arr[1]))
		*((x-arr[2])/(arr[0]-arr[2]))
		*((x-arr[3])/(arr[0]-arr[3])))
	return p_0;

def basis_poly_1(arr, x):
	p_1 = (((x-arr[0])/(arr[1]-arr[0]))
		*((x-arr[2])/(arr[1]-arr[2]))
		*((x-arr[3])/(arr[1]-arr[3])))
	return p_1;

def basis_poly_2(arr, x):
	p_2 = (((x-arr[0])/(arr[2]-arr[0]))
		*((x-arr[1])/(arr[2]-arr[1]))
		*((x-arr[3])/(arr[2]-arr[3])))
	return p_2;

def basis_poly_3(arr, x):
	p_3 = (((x-arr[0])/(arr[3]-arr[0]))
		*((x-arr[1])/(arr[3]-arr[1]))
		*((x-arr[2])/(arr[3]-arr[2])))
	return p_3;

def Lagrange_poly(y,arr,x):
	L = y[0]*basis_poly_0(arr,x) + y[1]*basis_poly_1(arr,x) + y[2]*basis_poly_2(arr,x) 
	+ y[3]*basis_poly_1(arr,x);
	return L;

def Newton_poly(y, arr, x):
	f_01 = (cos(arr[0]) - cos(arr[1]))/(arr[0] - arr[1])
	f_12 = (cos(arr[1]) - cos(arr[2]))/(arr[1] - arr[2])
	f_23 = (cos(arr[2]) - cos(arr[3]))/(arr[2] - arr[3])
	f_012 = (f_01 - f_12)/(arr[0] - arr[2])
	f_123 = (f_12 - f_23)/(arr[1] - arr[2])
	P = (cos(arr[0]) + (x - arr[0])*f_01 
		+ (x - arr[0])*(x- arr[1])*(f_01 - f_12)/(arr[0] - arr[2]) 
		+ (x - arr[0])*(x- arr[1])*(x - arr[2])*(f_012 - f_123)/(arr[0] - arr[3]))
	return P;

if __name__ == "__main__":
	print("Первый набор: ")
	y1 = [0,1,2,3]
	y2 = [0,1,2,3]
	for i in range(0,4):
		y1[i] = y_res(arr1[i]);
		print('x = {:6.4f}'.format(arr1[i]) + '\t y = {:6.4f}'.format(y1[i]))
	print("Второй набор: ")
	for i in range(0,4):
		y2[i] = y_res(arr2[i]);
		print('x = {:6.4f}'.format(arr2[i]) + '\t y = {:6.4f}'.format(y2[i]))

	x = sp.symbols('x')
	sp.init_printing(use_unicode=True)
	p0 = sp.simplify(((x-arr1[1])/(arr1[0]-arr1[1]))
		*((x-arr1[2])/(arr1[0]-arr1[2]))
		*((x-arr1[3])/(arr1[0]-arr1[3])))

	p1 = sp.simplify(((x-arr1[0])/(arr1[1]-arr1[0]))
		*((x-arr1[2])/(arr1[1]-arr1[2]))
		*((x-arr1[3])/(arr1[1]-arr1[3])))

	p2 = sp.simplify(((x-arr1[0])/(arr1[2]-arr1[0]))
		*((x-arr1[1])/(arr1[2]-arr1[1]))
		*((x-arr1[3])/(arr1[2]-arr1[3])))

	p3 = sp.simplify(((x-arr1[0])/(arr1[3]-arr1[0]))
		*((x-arr1[1])/(arr1[3]-arr1[1]))
		*((x-arr1[2])/(arr1[3]-arr1[2])))

	Lag1 = sp.simplify(y_res(arr1[0])*((x-arr1[1])/(arr1[0]-arr1[1]))
		*((x-arr1[2])/(arr1[0]-arr1[2]))
		*((x-arr1[3])/(arr1[0]-arr1[3])) + y_res(arr1[1])*
		((x-arr1[0])/(arr1[1]-arr1[0]))
		*((x-arr1[2])/(arr1[1]-arr1[2]))
		*((x-arr1[3])/(arr1[1]-arr1[3])) + y_res(arr1[2])*
		((x-arr1[0])/(arr1[2]-arr1[0]))
		*((x-arr1[1])/(arr1[2]-arr1[1]))
		*((x-arr1[3])/(arr1[2]-arr1[3])) + y_res(arr1[3])*
		((x-arr1[0])/(arr1[3]-arr1[0]))
		*((x-arr1[1])/(arr1[3]-arr1[1]))
		*((x-arr1[2])/(arr1[3]-arr1[2])))

	s0 = sp.simplify(((x-arr2[1])/(arr2[0]-arr2[1]))
		*((x-arr2[2])/(arr2[0]-arr2[2]))
		*((x-arr2[3])/(arr2[0]-arr2[3])))

	s1 = sp.simplify(((x-arr2[0])/(arr2[1]-arr2[0]))
		*((x-arr2[2])/(arr2[1]-arr2[2]))
		*((x-arr2[3])/(arr2[1]-arr2[3])))

	s2 = sp.simplify(((x-arr2[0])/(arr2[2]-arr2[0]))
		*((x-arr2[1])/(arr2[2]-arr2[1]))
		*((x-arr2[3])/(arr2[2]-arr2[3])))

	s3 = sp.simplify(((x-arr2[0])/(arr2[3]-arr2[0]))
		*((x-arr2[1])/(arr2[3]-arr2[1]))
		*((x-arr2[2])/(arr2[3]-arr2[2])))

	Lag2 = sp.simplify(y_res(arr2[0])*((x-arr2[1])/(arr2[0]-arr2[1]))
		*((x-arr2[2])/(arr2[0]-arr2[2]))
		*((x-arr2[3])/(arr2[0]-arr2[3])) + y_res(arr2[1])*
		((x-arr2[0])/(arr2[1]-arr2[0]))
		*((x-arr2[2])/(arr2[1]-arr2[2]))
		*((x-arr2[3])/(arr2[1]-arr2[3])) + y_res(arr2[2])*
		((x-arr2[0])/(arr2[2]-arr2[0]))
		*((x-arr2[1])/(arr2[2]-arr2[1]))
		*((x-arr2[3])/(arr2[2]-arr2[3])) + y_res(arr2[3])*
		((x-arr2[0])/(arr2[3]-arr2[0]))
		*((x-arr2[1])/(arr2[3]-arr2[1]))
		*((x-arr2[2])/(arr2[3]-arr2[2])))

	print("Нулевой полином первого набора: " + str(p0))
	print("Первый полином первого набора: " + str(p1))
	print("Второй полином первого набора: " + str(p2))
	print("Третий полином первого набора: " + str(p3))
	print("Многочлен Лагранджа для первого набора: " + str(Lag1)+'\n')
	print("Нулевой полином второго набора: " + str(s0))
	print("Первый полином второго набора: " + str(s1))
	print("Второй полином второго набора: " + str(s2))
	print("Третий полином второго набора: " + str(s3))
	print("Многочлен Лагранджа для второго набора: " + str(Lag2)+'\n')

	New1 = sp.simplify((cos(arr1[0]) + (x - arr1[0])*(cos(arr1[0]) - cos(arr1[1]))/(arr1[0] - arr1[1])
		+ (x - arr1[0])*(x- arr1[1])*((cos(arr1[0]) - cos(arr1[1]))/(arr1[0] - arr1[1]) - 
			(cos(arr1[1]) - cos(arr1[2]))/(arr1[1] - arr1[2]))/(arr1[0] - arr1[2]) + (x - arr1[0])*(x- arr1[1])*(x - arr1[2])*
		(((cos(arr1[0]) - cos(arr1[1]))/(arr1[0] - arr1[1]) - (cos(arr1[1]) - cos(arr1[2]))/(arr1[1] - arr1[2]))/(arr1[0] - arr1[2])
		 - ((cos(arr1[1]) - cos(arr1[2]))/(arr1[1] - arr1[2]) - (cos(arr1[2]) - cos(arr1[3]))/(arr1[2] - arr1[3]))/(arr1[1] - arr1[2]))/(arr1[0] - arr1[3])))

	New2 = sp.simplify((cos(arr2[0]) + (x - arr2[0])*(cos(arr2[0]) - cos(arr2[1]))/(arr2[0] - arr2[1])
		+ (x - arr2[0])*(x- arr2[1])*((cos(arr2[0]) - cos(arr2[1]))/(arr2[0] - arr2[1]) - 
			(cos(arr2[1]) - cos(arr2[2]))/(arr2[1] - arr2[2]))/(arr2[0] - arr2[2]) + (x - arr2[0])*(x- arr2[1])*(x - arr2[2])*
		(((cos(arr2[0]) - cos(arr2[1]))/(arr2[0] - arr2[1]) - (cos(arr2[1]) - cos(arr2[2]))/(arr2[1] - arr2[2]))/(arr2[0] - arr2[2])
		 - ((cos(arr2[1]) - cos(arr2[2]))/(arr2[1] - arr2[2]) - (cos(arr2[2]) - cos(arr2[3]))/(arr2[2] - arr2[3]))/(arr2[1] - arr2[2]))/(arr2[0] - arr2[3])))

	print("Многочлен Лагранджа для первого набора: " + str(New1) + '\n')
	print("Многочлен Лагранджа для второго набора: " + str(New2) + '\n')


	x = np.arange(-5, 5, 0.1);
	plt.plot(x, np.cos(x))

	#plt.plot(x, basis_poly_0(arr1,x))
	#plt.plot(x, -1.16105523959518*x**3 + 3.64756261112416*x**2 - 3.5014087480217*x + 1.0)

	#plt.plot(x, basis_poly_1(arr1,x))
	#plt.plot(x, x*(3.48316571878555*x**2 - 9.1189065278104*x + 5.72957795130823))

	#plt.plot(x, basis_poly_2(arr1,x))
	#plt.plot(x, x*(-3.48316571878555*x**2 + 7.29512522224832*x - 2.86478897565412))

	#plt.plot(x, basis_poly_3(arr1,x))
	#plt.plot(x, x*(1.16105523959518*x**2 - 1.82378130556208*x + 0.636619772367581))

	plt.plot(x, Lagrange_poly(y1,arr1,x))
	#plt.plot(x, 0.113871899071412*x**3 - 0.602079485571233*x**2 + 0.0281568229473721*x + 1.0)

	plt.plot(pi/4, Lagrange_poly(y1,arr1,pi/4), "ro")

	#plt.plot(x, basis_poly_0(arr2,x))
	#plt.plot(x, -0.928844191676145*x**3 + 3.16122092964094*x**2 - 3.31042281631142*x + 1.0)

	#plt.plot(x, basis_poly_1(arr2,x))
	#plt.plot(x, x*(2.32211047919036*x**2 - 6.68719812039429*x + 4.77464829275686))

	#plt.plot(x, basis_poly_2(arr2, x))
	#plt.plot(x, x*(-3.71537676670458*x**2 + 7.78146690373154*x - 3.05577490736439))

	#plt.plot(x, basis_poly_3(arr2, x))
	#plt.plot(x, x*(2.32211047919036*x**2 - 4.25548971297819*x + 1.59154943091895))

	#plt.plot(x, Lagrange_poly(y2,arr2,x))
	#plt.plot(x, 0.120552206742194*x**3 - 0.616070689239402*x**2 + 0.0336511557797228*x + 1.0)

	#plt.plot(pi/4, np.float_(Lagrange_poly(y2,arr2,pi/4)), "ro")

	#plt.plot(x , np.float_(Newton_poly(y1, arr1, x)))
	#plt.plot(x, -0.0416800078373142*x**3 - 0.357739121573067*x**2 - 0.057134053998416*x + 1.0)

	#plt.plot(pi/4 , np.float_(Newton_poly(y1, arr1,pi/4)), "ro")

	plt.plot(x , np.float_(Newton_poly(y2, arr2, x)))
	#plt.plot(x, 0.0768834287406928*x**3 - 0.536043473812291*x**2 + 0.00372104719959597*x + 1.0)

	#plt.plot(pi/4 , np.float_(Newton_poly(y2, arr2,pi/4)), "ro")

	origin = cos(pi/4)
	e1 = Lagrange_poly(y1,arr1,pi/4)
	print("Погрешность по Лагранджу в точке Pi/4 для первого набора: " + str(abs(origin - e1)))
	e2 = Lagrange_poly(y2,arr2,pi/4)
	print("Погрешность по Лагранджу в точке Pi/4 для второго набора: " + str(abs(origin - e2)))
	e3 = Newton_poly(y1, arr1,pi/4)
	print("Погрешность по Ньютону в точке Pi/4 для первого набора: " + str(abs(origin - e3)))
	e4 = Newton_poly(y2, arr2,pi/4)
	print("Погрешность по Ньютону в точке Pi/4 для второго набора: " + str(abs(origin - e4)))
	plt.axis([-pi/2,pi/2,-2,2])
	plt.show()