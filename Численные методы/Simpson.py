
h1 = 1;
h2 = 0.5;

x0 = 0;
xn = 4;

def F(x):
	return x/((3*x+4)**2);

def Rectangle(a,b,h):
	result = 0;
	n = int((b - a)/h);
	for i in range(n):
		result += F(a + h*(i + 0.5))
	result *= h;
	return result;

def Trapeze(a,b,h):
	result = 0;
	n = int((b - a)/h);
	for i in range(1,n-1):
		result += F(a +i*h);
	result += (F(a)+F(b))/2;
	result *= h;
	return result;

def Simpson(a,b,h):
	result = 0;
	chet = 0;
	nechet = 0;
	n = int((b - a)/h);
	for i in range(1,n-1,2): #вычисление нечетных
		nechet += F(a+i*h);
	nechet *= 4;
	for j in range(2,n-2,2): #вычисление четных
		chet += F(a+i*h);
	if( n-2 == 2):
		chet += F(2);
	chet *= 2;
	result = (F(a) +  F(b) + chet +nechet)*h/3;
	return result;
	

def RungeRomberg(res1, res2):
	return (abs(res1 -res2)/3);


if __name__ == "__main__":
	Rect1 = Rectangle(x0,xn,h1)
	Rect2 = Rectangle(x0,xn,h2)
	RungRc = RungeRomberg(Rect1, Rect2);
	print("Результат интегрирования: 0.0706993734577656")
	print("Метод прямоугольников для заданной функции c шагом h1: "
		+ str(Rect1))
	print("Метод прямоугольников для заданной функции c шагом h2: "
		+ str(Rect2))
	print("Погрешность для метода прямоугольников правилом Рунге: "
		+ str(RungRc) +'\n')

	Trap1 = Trapeze(x0,xn,h1)
	Trap2 = Trapeze(x0,xn,h2)
	RungTr = RungeRomberg(Trap1, Trap2)
	print("Метод трапеций для заданной функции c шагом h1: "
		+ str(Trap1))
	print("Метод трапеций для заданной функции c шагом h2: "
		+ str(Trap2))
	print("Погрешность для метода трапеций правилом Рунге: "
		+ str(RungTr) +'\n')

	Simps1 = Simpson(x0,xn,h1)
	Simps2 = Simpson(x0,xn,h2)
	RungSp = RungeRomberg(Simps1, Simps2)/5
	print("Метод Симпсона для заданной функции c шагом h1: "
		+ str(Simps1))
	print("Метод Симпсона для заданной функции c шагом h2: "
			+ str(Simps2))
	print("Погрешность для метода Симпсона правилом Рунге: "
		+ str(RungSp) +'\n')
