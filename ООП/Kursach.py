import matplotlib.pyplot as plt
import math as m

def useful_signal(step):
 	return m.sin(0.2*step)

def interference(step):
 	return m.cos(20*step)

class Filter(object):

	def __init__(self,kx,ky):
		self.kx = kx
		self.ky = ky

	def Calculation(self):
		x = []
		y = []
		xpr = []

		xp = useful_signal(0)
		v = interference(0)
		x3 = xp + v
		y3 = 0.0
		print("xp= {0:7.3f}   x= {0:7.3f}   y = {0:7.3f}".format((xp),(x3),(y3)))
		x.append(x3)
		y.append(y3)
		xpr.append(xp)

		xp = useful_signal(1)
		v = interference(1)
		x2 = xp + v
		y2 = 0.0
		print("xp= {0:7.3f}   x= {0:7.3f}   y = {0:7.3f}".format((xp),(x2),(y2)))
		x.append(x2)
		y.append(y2)
		xpr.append(xp)

		xp = useful_signal(2)
		v = interference(2)
		x1 = xp + v
		y1 = 0.0
		print("xp= {0:7.3f}   x= {0:7.3f}   y = {0:7.3f}".format((xp),(x1),(y1)))
		x.append(x1)
		y.append(y1)
		xpr.append(xp)

		for step in range(3,33):
			xp = useful_signal(step)
			v = interference(step)
			xr = xp + v
			yr = xr*kx[0] + x1*kx[1] + x2*kx[2] + x3*kx[3] - y1*ky[0] - y2*ky[1] - y3*ky[2]
			x3 = x2
			x2 = x1
			x1 = xr
			y3 = y2
			y2 = y1
			y1 = yr
			print("xp= {0:7.3f}   x= {0:7.3f}   y = {0:7.3f}".format((xp),(xr),(yr)))
			x.append(xr)
			y.append(yr)
			xpr.append(xp)

		t = []
		for i in range(0,33):
			t.append(i);

		plt.plot(t,xpr, label = 'Входной полезный сигнал')
		plt.plot(t,x, label ='Входной сигнал c учетом помех');
		plt.plot(t,y, label = 'Выходной сигнал');
		plt.legend(loc='upper right')
		plt.show()



if __name__ == "__main__":
	kx =  [0.0985, 0.2956, 0.2956, 0.0985];
	ky = [-0.5772, 0.4218, -0.0563];
	F = Filter(kx, ky)
	F.Calculation()
