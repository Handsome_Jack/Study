#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

class Coin
{
    private:
        int X, Y, R;
        double Fi;

    public:
        Coin(int, int, int, double);
        void PutX(int);
        int GetX();
        void PutFi(double);
        void Show();
        void Hide();
        void Slide(int);
        void Turn(double);
        void Roll(double);
};

Coin::Coin(int X, int Y, int R, double Fi)
{
    this ->X=X;
    this ->Y=Y;
    this -> R=R;
    this ->Fi=Fi;
}

int Coin ::GetX() { return (X); }
void Coin ::PutX(int X) { this -> X=X; }
void Coin ::PutFi(double Fi) { this -> Fi=Fi; }
void Coin ::Show()
{
    int Ry = R;
    int Rx = abs(R*cos(Fi));
    ellipse(X,Y,0,360,Rx,Ry);
}
void Coin::Hide()
{
    unsigned TempColor;
    TempColor=getcolor();
    setcolor(getbkcolor());
    Show();
    setcolor(TempColor);
}

void Coin::Slide(int DX)
{
    Hide();
    PutX(X+DX);
    Show();
}

void Coin::Turn(double DFi)
{
    Hide();
    PutFi(Fi+DFi);
    Show();
}
void Coin::Roll(double DFi)
{
    Turn(DFi);
    int DX=R*DFi;
    Slide(DX);
}

int main()
{
    int gdriver = DETECT, gmode, errorcode;
    initgraph(&gdriver, &gmode, "");
    errorcode = graphresult();
    if (errorcode != grOk)
    {
    cout<<"Graphics error: "<<grapherrormsg(errorcode)<<endl;
    cout<<"Press any key to halt:"<<endl;
    getch();
    return(1);
    }
    setcolor(15);
    Coin W1(320,100,50,0);
    Coin W2(100,250,50,0);
    Coin W3(100,400,50,0);

    W1.Show();
    W2.Show();
    W3.Show();
    getch();

    while(!kbhit())
    {
        W1.Turn(0.1);
        W2.Slide(50*0.1);
        W3.Roll(0.1);
        if(W2.GetX()>=700) W2.PutX(-50);
        if(W3.GetX()>=700) W3.PutX(-50);
        delay(100);
    };
    getch();
    closegraph();
    return(0);
};
