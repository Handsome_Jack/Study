{$N+}
program figures;
uses Crt, Graph, Objects;

type
    PointPtr = ^Point;
    Point = object
 
    public
        X,Y,Color: integer;
    public
        constructor Init(InitX, InitY, InitColor: integer);
        function Get_X: integer;
        function Get_Y: integer;
        procedure PutX(NewX: integer);
        procedure PutY(NewY: integer);
        function Get_Color: integer;
        procedure PutColor(NewColor: integer);
        procedure Show;
end;
 
type
    LinelPtr = ^Linel;
    Linel = object
    public
        P1: Point;
        P2: Point;
        constructor Init(InitP1, InitP2: Point);
        procedure Show;
end;
 
type
    SquarePtr = ^Square;
    Square = object
    public
        L : array[0..4] of Linel;
        constructor Init(InitL: array of Linel);
        procedure Show;
end;
 
type
    PyramidePtr = ^Pyramide;
    Pyramide = object
    public
        S : array [0..4] of Square;
        constructor Init(InitS: array of Square);
        procedure Show;
end;

constructor Point.Init(InitX, InitY, InitColor: integer);
    begin
        X:=InitX;
        Y:=InitY;
        Color:=InitColor;
    end;
 
constructor Linel.Init(InitP1, InitP2: Point);
    begin
        P1.X:=InitP1.Get_X;
        P1.Y:=InitP1.Get_Y;
        P2.X:=InitP2.Get_X;
        P2.Y:=InitP2.Get_Y
    end;
 
constructor Square.Init(InitL: array of Linel);
    var
        o: integer;
    begin
        for o:=0 to 4 do
            begin
                L[o].Init(InitL[o].P1,InitL[o].P2);
            end;
    end;
 
constructor Pyramide.Init(InitS: array of Square);
    var
        e: integer;
    begin
       for e:=0 to 4 do
            begin
                S[e].Init(InitS[e].L);
            end;
    end;
 
function Point.Get_X; begin Get_X:=X end;
function Point.Get_Y; begin Get_Y:=Y end;
function Point.Get_Color; begin Get_Color:=Color end;
procedure Point.PutX(NewX: integer); begin X:=NewX end;
procedure Point.PutY(NewY: integer); begin Y:=NewY end;
procedure Point.PutColor(NewColor: integer); begin Color:=NewColor end;

procedure Point.Show; begin PutPixel(X, Y, Color) end;
 
procedure Linel.Show;
    begin
        Line(P1.Get_X, P1.Get_Y, P2.Get_X, P2.Get_Y)
    end;
 
procedure Square.Show;
    var
        a: integer;
    begin
        for a:= 0 to 4 do
            begin
                L[a].Show;
            end;
    end;

procedure Pyramide.Show;
    var
        p: integer;
    begin
        for p:=0 to 4 do
            begin
                S[p].Show;
            end;
    end;
 
    var
        gdriver, gmode, errcode: integer;
        P1,P2: Point;
        L: Linel;
        L2: array [0..4] of Linel;
        S: Square;
        S2: array [0..4] of Square;
        R: Pyramide;
        DY, DX, start_x, finish_x, level: integer;
        i, k, j: integer;
    begin
        clrscr;
        gdriver:=detect;
        gmode:=detect;
        initgraph(gdriver, gmode,'');
        errcode:=GraphResult;
        if not (errcode = grOk) then
        begin
        writeln('Graphics error ', grapherrormsg(errcode));
        halt(1)
    end;
    P1.Init(100,250, 15);
    P2.Init(200,250, 15);
    P1.Show; P2.Show; readln;
    L.Init(P1,P2);
    L.Show; readln;
    DY := 0;
    for i:=0 to 4 do
        begin
            P1.Init(100, 250 + DY, 15);
            P2.Init(200, 250 + DY, 15);
            L2[i].Init(P1, P2);
            DY := DY +20;
        end;
    S.Init(L2);
    S.Show; readln;
    DY := 0;
    DX := 0;
    start_x := 250;
    finish_x := 450;
    level := 350;
    for k:=0 to 4 do
        begin
            for j:=0 to 4-k do
                begin
                    P1.Init(start_x + DX, level +DY, 15);
                    P2.Init(finish_x + DX, level +DY, 15);
                    L2[j].Init(P1, P2);
                    DX := DX + 20;
                    DY := DY - 35;
                end;
            S2[k].Init(L2);
            DX := 0;
            DY := 0;
            start_x := start_x + 15;
            finish_x := finish_x - 35;
            level := level - 50;
        end;
    R.Init(S2);
    R.Show; readln;
    readln;
    closegraph;
end.
