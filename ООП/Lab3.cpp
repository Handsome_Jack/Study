#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

class Point
{
	protected:
		int X,Y, Color;
	public:
		Point(int,int, int);
		Point();
		int GetX();
		int GetY();
		int GetColor();
		void PutX(int);
		void PutY(int);
		void PutColor(int);
		void Show();
		void Hide();
		void MoveTo(int,int);

};

class Line: public Point
{
	protected:
		double len, Ang;
	public:
		Line(int,int,int,double,double);
		Line(double, double);
		double GetAng();
		double GetLen();
		void PutAng(double);
		void PutLen(double);
		void Show();
		void Hide();
		void MoveTo(int,int);
		void Rotate(double);
		void Expand(double);
		int GetLength();
};

class Square: public Line
{
	public:
        Square(int,int,int,double,double);
        Square(double,double);
        void Show();
		void Hide();
		void MoveTo(int,int);
		void Rotate(double);
		void Expand(double);
		int GetSqaure();
};

class Pyramide: public Square
{
    protected:
        double H;
	public:
        Pyramide(int, int,int, double,double,double);
        Pyramide(double,double,double);
        void PutH(double);
        double GetH();
		void Show();
		void Hide();
		void MoveTo(int,int);
		void Rotate(double);
		void Expand(double);
		long int GetVolume();
};

Point::Point(int X, int Y, int Color) {this -> X=X; this -> Y=Y; this -> Color = Color;}
Point:: Point() {X=0.5*getmaxx(); Y=0.5*getmaxy(); Color=getmaxcolor();}
int Point::GetX() {return (X);}
int Point::GetY() {return (Y);}
int Point::GetColor() {return (Color);}
void Point::PutX(int X) {this -> X=X; }
void Point::PutY(int Y) {this -> Y=Y; }
void Point::PutColor(int Color) {this -> Color=Color;}
void Point::Show() {putpixel(X,Y,Color);}
void Point::Hide() {putpixel(X,Y,getbkcolor());}
void Point::MoveTo(int X_M, int Y_M) {Hide(); X = X_M; Y =Y_M; Show();}

Line::Line(int X, int Y, int Color, double len, double Ang):Point(X,Y, Color) { this -> len=len; this -> Ang=Ang;}
Line::Line(double len, double Ang) {this->len=len; this-> Ang=Ang;}
void Line::PutAng(double Ang) {this -> Ang=Ang;}
void Line::PutLen(double len) {this -> len=len;}
double Line::GetAng() {return Ang;}
double Line::GetLen() {return len;}


Square::Square(int X, int Y, int Color, double len, double Ang):Line(X,Y, Color,len,Ang){}
Square::Square(double len, double Ang):Line(len, Ang){};

Pyramide::Pyramide(int X, int Y, int Color, double len, double Ang, double H):Square(X,Y,Color, len, Ang)
{ this -> H=H;}
Pyramide::Pyramide(double len, double Ang, double H):Square(len, Ang) {this -> H=H;}
void Pyramide::PutH(double H) {this -> H=H;}
double Pyramide::GetH() {return H;}

void Line::Show()
{
	int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + len*(cos(Ang))/2;
	int Y1 = Y + len*(sin(Ang))/2;
	int X2 = X - len*(cos(Ang))/2;
	int Y2 = Y - len*(sin(Ang))/2;
	line(X1,Y1,X2,Y2);
	setcolor(TempColor);
}
void Line::Hide()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    Show();
    PutColor(TempColor);
}
void Line::MoveTo(int DX, int DY)
{
    Hide();
    PutX(X+DX);
    PutY(Y+DY);
    Show();
}
void Line::Rotate(double DAng)
{
    Hide();
    PutAng(Ang + DAng);
    Show();
}
void Line::Expand(double Angle)
{
    Hide();
    len += 5*cos(Angle);
    Show();
}
int Line::GetLength(){return len;}

void Square::Show()
{
    int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + len*cos(Ang)/2;
	int Y1 = Y + len/2 +sin(Ang)*len/4;
	int X2 = X + cos(Ang)*len/2;
	int Y2 = Y - len/2 + sin(Ang)*len/4;
	int X3 = X - len*(cos(Ang))/2;
	int Y3 = Y - len/2 -sin(Ang)*len/4;
	int X4 = X - len*(cos(Ang))/2;
	int Y4 = Y + len/2 - sin(Ang)*len/4;
	line(X1,Y1,X2,Y2);
	line(X2,Y2,X3,Y3);
	line(X3,Y3,X4,Y4);
	line(X4,Y4,X1,Y1);
	setcolor(TempColor);
}
void Square::Hide()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    Show();
    PutColor(TempColor);
}
void Square::MoveTo(int DX, int DY)
{
    Hide();
    PutX(X+DX);
    PutY(Y+DY);
    Show();
}
void Square::Rotate(double DAng)
{
    Hide();
    PutAng(Ang+DAng);
    Show();
}
void Square::Expand(double Angle)
{
    Hide();
    len += 5*cos(Angle);
    Show();
}
int Square::GetSqaure(){return len*len;}
void Pyramide::Show()
{
     int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + cos(Ang)*len - sin(Ang)*len;
	int Y1 = Y + sin(Ang)*len + cos(Ang)*len;
	int X2 = X + cos(Ang)*len + sin(Ang)*len;
	int Y2 = Y + sin(Ang)*len - cos(Ang)*len;
	int X3 = X - cos(Ang)*len + sin(Ang)*len;
	int Y3 = Y - sin(Ang)*len - cos(Ang)*len;
	int X4 = X - cos(Ang)*len - sin(Ang)*len;
	int Y4 = Y - sin(Ang)*len + cos(Ang)*len;
	int X5 = X;
	int Y5 = Y - H;
	line(X1,Y1,X2,Y2);
	line(X2,Y2,X3,Y3);
	line(X3,Y3,X4,Y4);
	line(X4,Y4,X1,Y1);
	line(X1,Y1,X5,Y5);
	line(X2,Y2,X5,Y5);
	line(X3,Y3,X5,Y5);
	line(X4,Y4,X5,Y5);
	setcolor(TempColor);
}
void Pyramide::Hide()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    Show();
    PutColor(TempColor);
}
void Pyramide::MoveTo(int DX,int DY)
{
    Hide();
    PutX(X+DX);
    PutY(Y+DY);
    Show();
}
void Pyramide::Rotate(double DAng)
{
    Hide();
    PutAng(Ang+DAng);
    Show();
}
void Pyramide::Expand(double Angle)
{
    Hide();
    len += 4*cos(Angle);
    H += 4*cos(Angle);
    Show();
}
long int Pyramide::GetVolume(){return H*len*len/3;}

int main()
{
	int gdriver = DETECT, gmode, errorcode;
	initgraph(&gdriver, &gmode, "");
	errorcode = graphresult();
	if(errorcode != grOk)
	{
		cout<<"Ошибка графики: "<<grapherrormsg(errorcode)<<endl;
		cout<<"Нажмите любую клавишу для прерывания: "<<endl;
		getch();	return (1);
	}
	setcolor(getmaxcolor());
	Point P1(25,25,15), P2;
	P1.Show();	getch();
	P2.Show(); getch();
	Line L1(50,100,15,100,0), L2(100,0);
	Square S1(250,300,27,100,0), S2(100,0);
	Pyramide R1(360,360,49, 100,0,150), R2(100,0,150);
    double Angle = 0;
	L1.Show(); L2.Show(); getch(); //cout<<"Lenght of line L1 = "<<L1.GetLength()<<endl; getch();
	S1.Show(); S2.Show(); getch();//cout<<"Square of square S1 = "<<S1.GetSqaure()<<endl;getch();
	R1.Show(); R2.Show(); getch();//cout<<"Volume of pyramide R1 = "<<R1.GetVolume()<<endl;getch();
	while(!kbhit()){
        Angle += 0.1;
        L1.Rotate(0.1);
        L2.Rotate(0.1);
        L1.MoveTo(5,0);
        L2.MoveTo(5,0);
        L1.Expand(Angle);
        L2.Expand(Angle);
        S1.MoveTo(5,0);
        S2.MoveTo(5,0);
        S1.Rotate(0.1);
        S2.Rotate(0.1);
        S1.Expand(Angle);
        S2.Expand(Angle);
        R1.MoveTo(5,0);
        R2.MoveTo(5,0);
        R1.Rotate(0.1);
        R2.Rotate(0.1);
        R1.Expand(Angle);
        R2.Expand(Angle);
        if(L1.GetX()>=700) L1.PutX(-80);
        if(L2.GetX()>=700) L2.PutX(-80);
        if(S1.GetX()>=700) S1.PutX(-80);
        if(S2.GetX()>=700) S2.PutX(-80);
        if(R1.GetX()>=800) R1.PutX(-120);
        if(R2.GetX()>=800) R2.PutX(-120);
        delay(100);
    }
	getch();
	closegraph();
	return 0;
}
