{$N+}
program figures;
uses Crt, Graph;
 
type
    PointPtr = ^Point;
    Point = object
 
    private
        X, Y, Color: integer;
    public
        constructor PointInit(InitX, InitY, InitColor: integer);
        function Get_X: integer;
        function Get_Y: integer;
        function Get_Color: integer;
        procedure PutX(NewX: integer);
        procedure PutY(NewY: integer);
        procedure PutColor(NewColor: integer);
        procedure Show;
        procedure Hide;
end;
 
type
    LinelPtr = ^Linel;
    Linel = object
 
    private
        X, Y, Color: integer;
        Ang, len: double;
    public
        constructor LinelInit(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
        function Get_X: integer;
        function Get_Y: integer;
        function Get_Color: integer;
        procedure PutX(NewX: integer);
        procedure PutY(NewY: integer);
        procedure PutColor(NewColor: integer);
        procedure PutAng(NewAng: double);
        procedure DrawLinel;
        procedure CleanLinel;
        procedure MoveLinel(DX, DY: integer);
        procedure RotateLinel(DAng: double);
        procedure ExpandLinel(DAng: double);
        function GetLength: integer;
end;
 
type
    SquarePtr = ^Square;
    Square = object
 
    private
        X, Y, Color: integer;
        Ang, len: double;
    public
        constructor SquareInit(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
        function Get_X: integer;
        function Get_Y: integer;
        function Get_Color: integer;
        procedure PutX(NewX: integer);
        procedure PutY(NewY: integer);
        procedure PutColor(NewColor: integer);
        procedure PutAng(NewAng: double);
        procedure DrawSquare;
        procedure CleanSquare;
        procedure MoveSquare(DX, DY: integer);
        procedure RotateSquare(DAng: double);
        procedure ExpandSquare(DAng: double);
        function GetSquare: integer;
end;
 
type
    PyramidePtr = ^Pyramide;
    Pyramide = object
 
    private
        X, Y, Color: integer;
        Ang, len, H: double;
    public
        constructor PyramideInit(InitX, InitY, InitColor: integer; InitAng, InitLen, InitH: double);
        function Get_X: integer;
        function Get_Y: integer;
        function Get_Color: integer;
        procedure PutX(NewX: integer);
        procedure PutY(NewY: integer);
        procedure PutColor(NewColor: integer);
        procedure PutAng(NewAng: double);
        procedure DrawPyramide;
        procedure CleanPyramide;
        procedure MovePyramide(DX, DY: integer);
        procedure RotatePyramide(DAng: double);
        procedure ExpandPyramide(DAng: double);
        function GetVolume: integer;
end;
 
constructor Point.PointInit(InitX, InitY, InitColor: integer);
    begin
        X:=InitX;
        Y:=InitY;
        Color:=InitColor;
    end;
 
constructor Linel.LinelInit(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
    begin
        X:=InitX;
        Y:=InitY;
        Color:=InitColor;
        Ang:=InitAng;
        len:=InitLen;
    end;
 
constructor Square.SquareInit(InitX, InitY, InitColor: integer; InitAng, InitLen: double);
    begin
        X:=InitX;
        Y:=InitY;
        Color:=InitColor;
        Ang:=InitAng;
        len:=InitLen;
    end;
 
constructor Pyramide.PyramideInit(InitX, InitY, InitColor: integer; InitAng, InitLen, InitH: double);
    begin
        X:=InitX;
        Y:=InitY;
        Color:=InitColor;
        Ang:=InitAng;
        len:=InitLen;
        H:=InitH
    end;
 
function Point.Get_X; begin Get_X:=X end;
function Point.Get_Y; begin Get_Y:=Y end;
function Point.Get_Color; begin Get_Color:=Color end;
procedure Point.PutX(NewX: integer); begin X:=NewX end;
procedure Point.PutY(NewY: integer); begin Y:=NewY end;
procedure Point.PutColor(NewColor: integer); begin Color:=NewColor end;
 
procedure Point. Show;
    begin
    putpixel(X,Y,Color);
    end;
 
procedure Point.Hide;
    begin
    putpixel(X,Y,getbkcolor);
    end;
 
function Linel.Get_X; begin Get_X:=X end;
function Linel.Get_Y; begin Get_Y:=Y end;
function Linel.Get_Color; begin Get_Color:=Color end;
procedure Linel.PutX(NewX: integer); begin X:=NewX end;
procedure Linel.PutY(NewY: integer); begin Y:=NewY end;
procedure Linel.PutColor(NewColor: integer); begin Color:=NewColor end;
procedure Linel.PutAng(NewAng: double); begin Ang:=NewAng end;
function Linel.GetLength; begin GetLength:=round(len) end;
 
procedure Linel.DrawLinel;
    var
        TempColor: integer;
        X1,Y1, X2,Y2: integer;
    begin
        TempColor:=GetColor;
        SetColor(Color);
        X1:= X + round(len*(Cos(Ang))/2.0);
        Y1:= Y + round(len*(Sin(Ang))/2.0);
        X2:= X - round(len*(Cos(Ang))/2.0);
        Y2:= Y - round(len*(Sin(Ang))/2.0);
        line(X1, Y1, X2, Y2);
        SetColor(TempColor)
    end;
 
procedure Linel.CleanLinel;
    var
        TempColor: integer;
    begin
        TempColor:=GetColor;
        PutColor(GetBkColor);
        DrawLinel;
        PutColor(TempColor);
    end;
 
procedure Linel.MoveLinel(DX,DY: integer);
    begin
        CleanLinel;
        PutX(X+DX);
        PutY(Y+DY);
        DrawLinel
    end;
 
procedure Linel.RotateLinel(DAng: double);
    begin
        CleanLinel;
        PutAng(Ang +DAng);
        DrawLinel
    end;
 
procedure Linel.ExpandLinel(DAng: double);
    begin
        CleanLinel;
        len:= len + 5*cos(DAng);
        DrawLinel
    end;
 
function Square.Get_X; begin Get_X:=X end;
function Square.Get_Y; begin Get_Y:=Y end;
function Square.Get_Color; begin Get_Color:=Color end;
procedure Square.PutX(NewX: integer); begin X:=NewX end;
procedure Square.PutY(NewY: integer); begin Y:=NewY end;
procedure Square.PutColor(NewColor: integer); begin Color:=NewColor end;
procedure Square.PutAng(NewAng: double); begin Ang:=NewAng end;
function Square.GetSquare; begin GetSquare:=round(len*len) end;
 
procedure Square.DrawSquare;
    var
        TempColor: integer;
        X1,Y1,X2,Y2,X3,Y3,X4,Y4: double;
    begin
        TempColor:=GetColor;
        SetColor(Color);
        X1 := X + len*cos(Ang)/2;
        Y1 := Y + len/2 +sin(Ang)*len/4;
        X2 := X + cos(Ang)*len/2;
        Y2 := Y - len/2 + sin(Ang)*len/4;
        X3 := X - len*(cos(Ang))/2;
        Y3 := Y - len/2 -sin(Ang)*len/4;
        X4 := X - len*(cos(Ang))/2;
        Y4 := Y + len/2 - sin(Ang)*len/4;
        Line(round(X1),round(Y1),round(X2),round(Y2));
        Line(round(X2),round(Y2),round(X3),round(Y3));
        Line(round(X3),round(Y3),round(X4),round(Y4));
        Line(round(X4),round(Y4),round(X1),round(Y1));
        SetColor(TempColor)
    end;
 
procedure Square.CleanSquare;
    var
        TempColor: integer;
    begin
        TempColor:=Get_Color;
        PutColor(GetBkColor);
        DrawSquare;
        PutColor(TempColor);
    end;
 
procedure Square.MoveSquare(DX,DY: integer);
    begin
        CleanSquare;
        PutX(X+DX);
        PutY(Y+DY);
        DrawSquare
    end;
 
procedure Square.RotateSquare(DAng: double);
    begin
        CleanSquare;
        PutAng(Ang +DAng);
        DrawSquare
    end;
 
procedure Square.ExpandSquare(DAng: double);
    begin
        CleanSquare;
        len:= len + 5*cos(DAng);
        DrawSquare
    end;
 
function Pyramide.Get_X; begin Get_X:=X end;
function Pyramide.Get_Y; begin Get_Y:=Y end;
function Pyramide.Get_Color; begin Get_Color:=Color end;
procedure Pyramide.PutX(NewX: integer); begin X:=NewX end;
procedure Pyramide.PutY(NewY: integer); begin Y:=NewY end;
procedure Pyramide.PutColor(NewColor: integer); begin Color:=NewColor end;
procedure Pyramide.PutAng(NewAng: double); begin Ang:=NewAng end;
function Pyramide.GetVolume; begin GetVolume:=round(len*len*H/3.0) end;
 
procedure Pyramide.DrawPyramide;
    var
        TempColor: integer;
        X1,Y1,X2,Y2,X3,Y3,X4,Y4,X5,Y5: integer;
    begin
        TempColor:=GetColor;
        SetColor(Color);
        X1:= X + round(cos(Ang)*len - sin(Ang)*len);
        Y1:= Y + round(sin(Ang)*len + cos(Ang)*len);
        X2:= X + round(cos(Ang)*len + sin(Ang)*len);
        Y2:= Y + round(sin(Ang)*len - cos(Ang)*len);
        X3:= X - round(cos(Ang)*len + sin(Ang)*len);
        Y3:= Y - round(sin(Ang)*len - cos(Ang)*len);
        X4:= X - round(cos(Ang)*len - sin(Ang)*len);
        Y4:= Y - round(sin(Ang)*len + cos(Ang)*len);
        X5:= X;
        Y5:= Y - round(H);
        Line(X1,Y1,X2,Y2);
        Line(X2,Y2,X4,Y4);
        Line(X3,Y3,X1,Y1);
        Line(X4,Y4,X3,Y3);
        Line(X1,Y1,X5,Y5);
        Line(X2,Y2,X5,Y5);
        Line(X3,Y3,X5,Y5);
        Line(X4,Y4,X5,Y5);
        SetColor(TempColor)
    end;
 
procedure Pyramide.CleanPyramide;
    var
        TempColor: integer;
    begin
        TempColor:=Get_Color;
        PutColor(GetBkColor);
        DrawPyramide;
        PutColor(TempColor);
    end;
 
procedure Pyramide.MovePyramide(DX,DY: integer);
    begin
        CleanPyramide;
        PutX(X+DX);
        PutY(Y+DY);
        DrawPyramide
    end;
 
procedure Pyramide.RotatePyramide(DAng: double);
    begin
        CleanPyramide;
        PutAng(Ang +DAng);
        DrawPyramide
    end;
 
procedure Pyramide.ExpandPyramide(DAng: double);
    begin
        CleanPyramide;
        len:= len + 5*cos(DAng);
        H:= H + 5*cos(DAng);
        DrawPyramide
    end;
 
    var
        gdriver, gmode, errcode: integer;
        P1: Point;
        L1: Linel;
        S1: Square;
        R1: Pyramide;
        Angle: double;
    begin
        clrscr;
        gdriver:=detect;
        gmode:=detect;
        initgraph(gdriver, gmode,'');
        errcode:=GraphResult;
        if not (errcode = grOk) then
        begin
        writeln('Graphics error ', grapherrormsg(errcode));
        halt(1)
    end;
    setcolor(15);
    P1.PointInit(25,25,15);
    L1.LinelInit(50,100,15,0,100);
    S1.SquareInit(250,300,27,0,100);
    R1.PyramideInit(400,400,49,0,80,150);
    P1.Show;
    readln;
    L1.DrawLinel;
    readln;
    S1.DrawSquare;
    readln;
    R1.DrawPyramide;
    readln;
    repeat
        Angle := Angle + 0.1;
        L1.RotateLinel(0.1);
        L1.MoveLinel(5,0);
        L1.ExpandLinel(Angle);
        S1.MoveSquare(5,0);
        S1.RotateSquare(0.1);
        S1.ExpandSquare(Angle);
        R1.MovePyramide(5,0);
        R1.RotatePyramide(0.1);
        R1.ExpandPyramide(Angle);
        if L1.Get_X>=700 then L1.PutX(-50);
        if S1.Get_X>=700 then S1.PutX(-50);
        if R1.Get_X>=800 then R1.PutX(-100);
        delay(100);
    until KeyPressed;
closegraph;
end.