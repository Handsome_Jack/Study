#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

class Location
{
	protected:
		int X, Y; 
	public:
		Location(int,int);
		int GetX();
		int GetY();
		void PutX(int);
		void PutY(int);
		virtual void Show() = 0;
		virtual void Hide() = 0;
};
class Point: public Location
{
	protected:
		int Color;
	public:
		Point(int,int, int);
		int GetColor();
		void PutColor(int);
    	void Show();
		void Hide();
};

class Line: public Point
{
	protected:
		double len, Ang;
	public:
		Line(int,int,int,double,double);
		double GetAng();
		double GetLen();
		void PutAng(double);
		void PutLen(double);
		void Show();
		int GetLength();
};

class Square: public Line
{
	public:
        Square(int,int,int,double,double);
        void Show();
		int GetSqaure();
};

class Pyramide: public Square
{
    protected:
        double H;
	public:
        Pyramide(int, int,int, double,double,double);
        void PutH(double);
        double GetH();
        void Show();
        long int GetVolume();
};

Location::Location(int X, int Y) {this -> X=X; this -> Y=Y;}

int Location::GetX() {return (X);}

int Location::GetY() {return (Y);}

void Location::PutX(int X) {this -> X=X; }

void Location::PutY(int Y) {this -> Y=Y; }

Point::Point(int X, int Y, int Color):Location(X,Y) {this -> Color = Color;}

int Point::GetColor() {return (Color);}

void Point::PutColor(int Color) {this -> Color=Color;}

void Point::Show() {putpixel(X,Y,Color);}

void Point::Hide()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    Show();
    PutColor(TempColor);
}

Line::Line(int X, int Y, int Color, double len, double Ang):Point(X,Y, Color) 
{ this -> len=len; this -> Ang=Ang;}

void Line::PutAng(double Ang) {this -> Ang=Ang;}

void Line::PutLen(double len) {this -> len=len;}

double Line::GetAng() {return Ang;}

double Line::GetLen() {return len;}

Square::Square(int X, int Y, int Color, double len, double Ang):Line(X,Y, Color,len,Ang){}

Pyramide::Pyramide(int X, int Y, int Color, double len, double Ang, double H)
:Square(X,Y,Color, len, Ang)
{ this -> H=H;}

void Pyramide::PutH(double H) {this -> H=H;}

double Pyramide::GetH() {return H;}

void Line::Show()
{
	int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + len*(cos(Ang))/2;
	int Y1 = Y + len*(sin(Ang))/2;
	int X2 = X - len*(cos(Ang))/2;
	int Y2 = Y - len*(sin(Ang))/2;
	line(X1,Y1,X2,Y2);
	setcolor(TempColor);
}

int Line::GetLength(){return len;}

void Square::Show()
{
    int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + len*cos(Ang)/2;
	int Y1 = Y + len/2 +sin(Ang)*len/4;
	int X2 = X + cos(Ang)*len/2;
	int Y2 = Y - len/2 + sin(Ang)*len/4;
	int X3 = X - len*(cos(Ang))/2;
	int Y3 = Y - len/2 -sin(Ang)*len/4;
	int X4 = X - len*(cos(Ang))/2;
	int Y4 = Y + len/2 - sin(Ang)*len/4;
	line(X1,Y1,X2,Y2);
	line(X2,Y2,X3,Y3);
	line(X3,Y3,X4,Y4);
	line(X4,Y4,X1,Y1);
	setcolor(TempColor);
}
int Square::GetSqaure(){ return len*len;}

void Pyramide::Show()
{
    int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + cos(Ang)*len - sin(Ang)*len;
	int Y1 = Y + sin(Ang)*len + cos(Ang)*len;
	int X2 = X + cos(Ang)*len + sin(Ang)*len;
	int Y2 = Y + sin(Ang)*len - cos(Ang)*len;
	int X3 = X - cos(Ang)*len + sin(Ang)*len;
	int Y3 = Y - sin(Ang)*len - cos(Ang)*len;
	int X4 = X - cos(Ang)*len - sin(Ang)*len;
	int Y4 = Y - sin(Ang)*len + cos(Ang)*len;
	int X5 = X;
	int Y5 = Y - H;
	line(X1,Y1,X2,Y2);
	line(X2,Y2,X3,Y3);
	line(X3,Y3,X4,Y4);
	line(X4,Y4,X1,Y1);
	line(X1,Y1,X5,Y5);
	line(X2,Y2,X5,Y5);
	line(X3,Y3,X5,Y5);
	line(X4,Y4,X5,Y5);
	setcolor(TempColor);
}

long int Pyramide::GetVolume(){return H*len*len/3;}

void MoveTo(int DX,int DY, Location *pL) 
{
	pL -> Hide();
	pL -> PutX(DX); 
	pL -> PutY(DY); 
	pL -> Show();
}

void Rotate(double DAng, Line *pL)
{
	pL -> Hide();
    pL -> PutAng(DAng);
    pL -> Show();
}

void Expand(double Angle, Line *pL)
{
	pL -> Hide();
    pL -> PutLen((pL -> GetLen()) + 5*cos(Angle));
    pL -> Show();
}

int main()
{
	int gdriver = DETECT, gmode, errorcode;
	initgraph(&gdriver, &gmode, "");
	errorcode = graphresult();
	if(errorcode != grOk)
	{
		cout<<"Ошибка графики: "<<grapherrormsg(errorcode)<<endl;
		cout<<"Нажмите любую клавишу для прерывания: "<<endl;
		getch();	return (1);
	}
	setcolor(getmaxcolor());
	Point P1(150,250,15);
	Line L1(50,100,15,100,0);
	Square S1(250,300,27,100,0);
	Pyramide R1(360,360,49, 100,0,150);
  	double Angle = 0;
  	P1.Show(); getch();
	L1.Show(); getch(); //cout<<"Lenght of line L1 = "<<L1.GetLength()<<endl; getch();
	S1.Show(); getch();//cout<<"Square of square S1 = "<<S1.GetSqaure()<<endl;getch();
	R1.Show(); //cout<<"Volume of pyramide R1 = "<<R1.GetVolume()<<endl;getch();
	getch();
	Location *pL;
	pL = &L1;
	MoveTo(100,100,pL);
	pL = &S1;
	MoveTo(350,200,pL);
	pL = &R1;
	MoveTo(400,300,pL);
	getch();
	Rotate(35, &L1);
	Rotate(35, &S1);
	Rotate(35, &R1);
	getch();
	Angle =155;
	Expand(Angle, &L1);
	Expand(Angle, &S1);
	Expand(Angle, &R1);
	getch();
	closegraph();
	return 0;
}
                  