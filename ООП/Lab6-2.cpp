#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

class Point
{
	private:
		int X,Y,Color;
	public:
		Point(int,int, int);
		Point(){}
		int GetX();
		int GetY();
		void PutX(int);
		void PutY(int);
		int GetColor();
		void PutColor(int);
   		void Show();
};

class Line
{
	public:
		Point P1, P2;
		Line(Point, Point);
		Line(){}
		void Show();
};

class Square: public Line
{
	public:
	  	Point P3, P4;
		Square(Point, Point, Point, Point);
		Square(){}
	    void Show();
};

class Pyramide: public Square
{
	public:
		Point P5;
	  	Pyramide(Point, Point, Point, Point, Point);
	  	void Show();
};

Point::Point(int X, int Y, int Color)
{
	this -> X = X;
	this -> Y = Y;
	this -> Color = Color;
}

int Point::GetX() {return (X);}

int Point::GetY() {return (Y);}

void Point::PutX(int X) {this -> X=X; }

void Point::PutY(int Y) {this -> Y=Y; }

int Point::GetColor() {return (Color);}

void Point::PutColor(int Color) {this -> Color=Color;}

void Point::Show() {putpixel(X,Y,Color);}

Line::Line(Point P1, Point P2)
{
	this -> P1 = P1;
	this -> P2 = P2;
}

Square::Square(Point P1, Point P2, Point P3, Point P4):Line(P1,P2)
{
	this -> P3 = P3;
	this -> P4 = P4;
}

Pyramide::Pyramide(Point P1, Point P2, Point P3, Point P4, Point P5):
Square(P1,P2,P3,P4)
{ 
	this -> P5 = P5;
}

void Line::Show()
{
	line(P1.GetX(), P1.GetY(), P2.GetX(), P2.GetY());
}

void Square::Show()
{
	line(P1.GetX(), P1.GetY(), P2.GetX(), P2.GetY());
	line(P2.GetX(), P2.GetY(), P3.GetX(), P3.GetY());
	line(P3.GetX(), P3.GetY(), P4.GetX(), P4.GetY());
	line(P4.GetX(), P4.GetY(), P1.GetX(), P1.GetY());
}

void Pyramide::Show()
{
	line(P1.GetX(), P1.GetY(), P2.GetX(), P2.GetY());
	line(P2.GetX(), P2.GetY(), P3.GetX(), P3.GetY());
	line(P3.GetX(), P3.GetY(), P4.GetX(), P4.GetY());
	line(P4.GetX(), P4.GetY(), P4.GetX(), P4.GetY());
	line(P1.GetX(), P1.GetY(), P5.GetX(), P5.GetY());
	line(P2.GetX(), P2.GetY(), P5.GetX(), P5.GetY());
	line(P3.GetX(), P3.GetY(), P5.GetX(), P5.GetY());
	line(P4.GetX(), P4.GetY(), P5.GetX(), P5.GetY());
}

int main()
{
	int gdriver = DETECT, gmode, errorcode;
	initgraph(&gdriver, &gmode, "");
	errorcode = graphresult();
	if(errorcode != grOk)
	{
		cout<<"����� �������: "<<grapherrormsg(errorcode)<<endl;
		cout<<"������� ����� ������� ��� ����������: "<<endl;
		getch();	return (1);
	}
	setcolor(getmaxcolor());
	Point P1(100,250, 8);
	Point P2(200,250, 8);
	P1.Show();
	P2.Show();
	getch();
	Line L(P1,P2);
	L.Show();
	getch();
	Point P3(200,150,8);
	Point P4(100,150,8);
	Point P5(150,100,8);
	Square S(P1,P2,P3,P4);
	S.Show();
	getch();
	Pyramide R2(P1,P2,P3,P4,P5);
	R2.Show();
	getch();
	closegraph();
	return 0;
}
