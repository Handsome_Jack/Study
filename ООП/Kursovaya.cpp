﻿#include <iostream>
#include <cmath>
#include <stdio.h>

double useful_signal(int step)
    {return sin(0.2*step);}

double noise(int step)
    {return cos(2.0*step);}

class Filter
{
    private:
        std::vector<double> kx;
        std::vector<double> ky;
    public:
        Filter(std::vector<double> kx, std::vector<double> ky);
        int Get_kx_number(){return kx.size();}
        int Get_ky_number(){return ky.size();}
        void Calculation();
};

Filter::Filter(std::vector<double> kx, std::vector<double> ky)
{
     this -> kx = kx;
     this -> ky = ky;
}

void Filter::Calculation()
{
    double xp,v;
    double x, x1,x2,x3;
    double y, y1, y2, y3;

    xp = useful_signal(0);
    v = noise(0);
    x3 = xp + v;
    y3 = 0;
    printf("\n xp= %10.3f   x= %10.3f   y = %10.3f",xp,x3,y3);

    xp = useful_signal(1);
    v = noise(1);
    x2 = xp + v;
    y2 = 0;
    printf("\n xp= %10.3f   x= %10.3f   y = %10.3f",xp,x2,y2);

    xp = useful_signal(2);
    v = noise(2);
    x1 = xp + v;
    y1 = 0;
    printf("\n xp= %10.3f   x= %10.3f   y = %10.3f",xp,x1,y1);

    for(int step = 3; step<33; step++)
    {
        xp = useful_signal(step);
        v = noise(step);
        x = xp +v;
        y = x*kx[0] + x1*kx[1] + x2*kx[2] + x3*kx[3] - y1*ky[0] - y2*ky[1] - y3*ky[2];
        x3 = x2;
        x2 = x1;
        x1 = x;
        y3 = y2;
        y2 = y1;
        printf("\n xp= %10.3f   x= %10.3f   y = %10.3f",xp,x,y);
    }
}

int main()
{
    std::vector<double> kx = {0.985, 0.2956, 0.2956, 0.0985};
    std::vector<double> ky = {-0.5772, 0.4218, -0.0563};
    Filter F(kx,ky);
    std::cout<<F.Get_kx_number()<<'\t'<<F.Get_ky_number();
    F.Calculation();
    return 0;
}
