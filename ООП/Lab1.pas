{$N+}
program figures;
uses Crt, Graph;

type
	CoinPtr = ^Coin;
	Coin = object

	private
		X, Y: integer;
		R: integer;
		Fi: double;

	public
		constructor Init(InitX, InitY, InitR: integer; InitFi: double);
		function Get_X: integer;
		procedure PutX(NewX: integer);
		procedure PutFi(NewFi: double);
		procedure Show;
		procedure Hide;
		procedure Slide(DX: integer);
		procedure Turn(DFi: double);
		procedure Roll(DFi: double);
end;

constructor Coin.Init(InitX, InitY, InitR: integer; InitFi: double);
	begin
		X:=InitX;
		Y:=InitY;
		R:=InitR;
		Fi:=InitFi
	end;

function Coin.Get_X; begin Get_X:=X end;

procedure Coin.PutX(NewX: integer); begin X:=NewX end;

procedure Coin.PutFi(NewFi: double); begin Fi:=NewFi end;

procedure Coin. Show;
    var
        Rx, Ry : word;
        StAngle, EndAngle : word;
	begin
        StAngle := 0;
        EndAngle := 360;
        Ry := R;
        Rx := round(Abs(R*cos(Fi)));
		Ellipse(X, Y, StAngle, EndAngle, Rx, Ry)
	end;
procedure Coin. Hide;
	var
		TempColor: word;
	begin
		TempColor:=GetColor;
		SetColor(GetBkColor);
		Show;
		SetColor(TempColor);
	end;

procedure Coin. Slide(DX: integer);
	begin
		Hide;
		PutX(X+DX);
		Show
	end;

procedure Coin. Turn(DFi: double);
	begin
		Hide;
		PutFi(Fi +DFi);
		Show
	end;

procedure Coin. Roll(DFi: double);
	var
		DX:integer;
	begin
		Turn(DFi);
		DX:=round(DFi*R);
		Slide(DX)
	end;

	var
		gdriver, gmode, errcode: integer;
		W1,W2,W3: Coin;
	begin
		clrscr;
		gdriver:=detect;
		gmode:=detect;
		initgraph(gdriver, gmode,'');
		errcode:=GraphResult;
		if not (errcode = grOk) then
		begin
		writeln('Graphics error ', grapherrormsg(errcode));
		halt(1)
	end;
	setcolor(15);
	W1.Init(300, 100, 50, 0);
	W2.Init(100, 250, 50, 0);
	W3.Init(100, 400, 50, 0);
	W1.Show;
	W2.Show;
	W3.Show;
	repeat
		W1.Turn(0.1); {вращение 1-го колеса}
		W2.Slide(5); { скольжение 2-го колеса}
		W3.Roll(0.1); { перекатывание 3-го колеса}
		if W2.Get_X>=700 then W2.PutX(-50); { защита от выхода объектов}
		if W3.Get_X>=700 then W3.PutX(-50); { за пределы экрана}
		delay(10); {задержка изображения на экране}
	until KeyPressed;
closegraph;
end.

