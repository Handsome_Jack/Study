#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

class Point
{
	protected:
		int X,Y, Color;
	public:
		Point(int,int, int);
		int GetX();
		int GetY();
		int GetColor();
		void PutX(int);
		void PutY(int);
		void PutColor(int);
		void Show();
		void Hide();
		void MoveTo(int,int);

};

class Line: public Point
{
	protected:
		double len;
		double Ang;
	public:
		Line(int,int,int,double,double);
		void DrawLine();
		void CleanLine();
		void PutAng(double);
		void MoveLine(int,int);
		void RotateLine(double);
		void ExpandLine(double);
		int GetLength();
};

class Square: public Line
{
	public:
        Square(int,int,int,double,double);
        void DrawSquare();
		void CleanSqaure();
		void MoveSquare(int,int);
		void RotateSquare(double);
		void ExpandSquare(double);
		int GetSqaure();
};

class Pyramide: public Square
{
    protected:
        double H;
	public:
        Pyramide(int, int,int, double,double,int);
		void DrawPyramide();
		void CleanPyramide();
		void MovePyramide(int,int);
		void RotatePyramide(double);
		void ExpandPyramide(double);
		long int GetVolume();
};

Point::Point(int X, int Y, int Color) {this -> X=X; this -> Y=Y; this -> Color = Color;}
int Point::GetX() {return (X);}
int Point::GetY() {return (Y);}
int Point::GetColor() {return (Color);}
void Point::PutX(int X) {this -> X=X; }
void Point::PutY(int Y) {this -> Y=Y; }
void Point::PutColor(int Color) {this -> Color=Color;}
void Point::Show() {putpixel(X,Y,Color);}
void Point::Hide() {putpixel(X,Y,getbkcolor());}
void Point::MoveTo(int X_M, int Y_M) {Hide(); X = X_M; Y =Y_M; Show();}

Line::Line(int X, int Y, int Color, double len, double Ang):Point(X,Y, Color) { this -> len=len; this -> Ang=Ang;}
void Line::PutAng(double Ang) {this -> Ang=Ang;}

Square::Square(int X, int Y, int Color, double len, double Ang):Line(X,Y, Color,len,Ang){}

Pyramide::Pyramide(int X, int Y, int Color, double len, double Ang, int H):Square(X,Y,Color, len, Ang)
{ this -> H=H;}

void Line::DrawLine()
{
	int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + len*(cos(Ang))/2;
	int Y1 = Y + len*(sin(Ang))/2;
	int X2 = X - len*(cos(Ang))/2;
	int Y2 = Y - len*(sin(Ang))/2;
	line(X1,Y1,X2,Y2);
	setcolor(TempColor);
}
void Line::CleanLine()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    DrawLine();
    PutColor(TempColor);
}
void Line::MoveLine(int DX, int DY)
{
    CleanLine();
    PutX(X+DX);
    PutY(Y+DY);
    DrawLine();
}
void Line::RotateLine(double DAng)
{
    CleanLine();
    PutAng(Ang + DAng);
    DrawLine();
}
void Line::ExpandLine(double Angle)
{
    CleanLine();
    len += 5*cos(Angle);
    DrawLine();
}
int Line::GetLength(){return len;}

void Square::DrawSquare()
{
    int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + len*cos(Ang)/2;
	int Y1 = Y + len/2 +sin(Ang)*len/4;
	int X2 = X + cos(Ang)*len/2;
	int Y2 = Y - len/2 + sin(Ang)*len/4;
	int X3 = X - len*(cos(Ang))/2;
	int Y3 = Y - len/2 -sin(Ang)*len/4;
	int X4 = X - len*(cos(Ang))/2;
	int Y4 = Y + len/2 - sin(Ang)*len/4;
	line(X1,Y1,X2,Y2);
	line(X2,Y2,X3,Y3);
	line(X3,Y3,X4,Y4);
	line(X4,Y4,X1,Y1);
	setcolor(TempColor);
}
void Square::CleanSqaure()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    DrawSquare();
    PutColor(TempColor);
}
void Square::MoveSquare(int DX, int DY)
{
    CleanSqaure();
    PutX(X+DX);
    PutY(Y+DY);
    DrawSquare();
}
void Square::RotateSquare(double DAng)
{
    CleanSqaure();
    PutAng(Ang+DAng);
    DrawSquare();
}
void Square::ExpandSquare(double Angle)
{
    CleanSqaure();
    len += 5*cos(Angle);
    DrawSquare();
}
int Square::GetSqaure(){return len*len;}
void Pyramide::DrawPyramide()
{
     int TempColor = getcolor();
	setcolor(Color);
	int X1 = X + cos(Ang)*len - sin(Ang)*len;
	int Y1 = Y + sin(Ang)*len + cos(Ang)*len;
	int X2 = X + cos(Ang)*len + sin(Ang)*len;
	int Y2 = Y + sin(Ang)*len - cos(Ang)*len;
	int X3 = X - cos(Ang)*len + sin(Ang)*len;
	int Y3 = Y - sin(Ang)*len - cos(Ang)*len;
	int X4 = X - cos(Ang)*len - sin(Ang)*len;
	int Y4 = Y - sin(Ang)*len + cos(Ang)*len;
	int X5 = X;
	int Y5 = Y - H;
	line(X1,Y1,X2,Y2);
	line(X2,Y2,X3,Y3);
	line(X3,Y3,X4,Y4);
	line(X4,Y4,X1,Y1);
	line(X1,Y1,X5,Y5);
	line(X2,Y2,X5,Y5);
	line(X3,Y3,X5,Y5);
	line(X4,Y4,X5,Y5);
	setcolor(TempColor);
}
void Pyramide::CleanPyramide()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    DrawPyramide();
    PutColor(TempColor);
}
void Pyramide::MovePyramide(int DX,int DY)
{
    CleanPyramide();
    PutX(X+DX);
    PutY(Y+DY);
    DrawPyramide();
}
void Pyramide::RotatePyramide(double DAng)
{
    CleanPyramide();
    PutAng(Ang+DAng);
    DrawPyramide();
}
void Pyramide::ExpandPyramide(double Angle)
{
    CleanPyramide();
    len += 4*cos(Angle);
    H += 4*cos(Angle);
    DrawPyramide();
}
long int Pyramide::GetVolume(){return H*len*len/3;}

int main()
{
	int gdriver = DETECT, gmode, errorcode;
	initgraph(&gdriver, &gmode, "");
	errorcode = graphresult();
	if(errorcode != grOk)
	{
		cout<<"Ошибка графики: "<<grapherrormsg(errorcode)<<endl;
		cout<<"Нажмите любую клавишу для прерывания: "<<endl;
		getch();	return (1);
	}
	setcolor(getmaxcolor());
	Point P1(25,25,15);
	P1.Show();	getch();
	Line L1(50,100,15,100,0);
	Square S1(250,300,27,100,0);
	Pyramide R1(400,400,49, 80,0,150);
    double Angle = 0;
	L1.DrawLine(); cout<<"Lenght of line L1 = "<<L1.GetLength()<<endl; getch();
	S1.DrawSquare(); cout<<"Square of square S1 = "<<S1.GetSqaure()<<endl;getch();
	R1.DrawPyramide(); cout<<"Volume of pyramide R1 = "<<R1.GetVolume()<<endl;getch();
	while(!kbhit()){
        Angle += 0.1;
        L1.RotateLine(0.1);
        L1.MoveLine(5,0);
        L1.ExpandLine(Angle);
        S1.MoveSquare(5,0);
        S1.RotateSquare(0.1);
        S1.ExpandSquare(Angle);
        R1.MovePyramide(5,0);
        R1.RotatePyramide(0.1);
        R1.ExpandPyramide(Angle);
        if(L1.GetX()>=700) L1.PutX(-80);
        if(S1.GetX()>=700) S1.PutX(-80);
        if(R1.GetX()>=800) R1.PutX(-100);
        delay(100);
    }
	getch();
	closegraph();
	return 0;
}

