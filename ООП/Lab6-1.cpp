#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

class Point
{
	private:
		int X,Y,Color;
	public:
		Point(int,int, int);
		Point(){}
		int GetX();
		int GetY();
		void PutX(int);
		void PutY(int);
		int GetColor();
		void PutColor(int);
    	void Show();
		void Hide();
};

class Line
{
	public:
		Point P1, P2;
		Line(Point, Point);
		Line(){}
		void Show();
};

class Square
{
	public:
	  	Line L[5];
		Square(Line []);
		Square(){}
	    void Show();
};

class Pyramide
{
	public:
		Square S[5];
	  	Pyramide(Square [5]);
	  	void Show();
};

Point::Point(int X, int Y, int Color)
{
	this -> X = X;
	this -> Y = Y;
	this -> Color = Color;
}

int Point::GetX() {return (X);}

int Point::GetY() {return (Y);}

void Point::PutX(int X) {this -> X=X; }

void Point::PutY(int Y) {this -> Y=Y; }

int Point::GetColor() {return (Color);}

void Point::PutColor(int Color) {this -> Color=Color;}

void Point::Show() {putpixel(X,Y,Color);}

void Point::Hide()
{
    int TempColor = Color;
    PutColor(getbkcolor());
    Show();
    PutColor(TempColor);
}

Line::Line(Point P1, Point P2)
{
	this -> P1 = P1;
	this -> P2 = P2;
}

Square::Square(Line PL[])
{
	for(int i = 0; i <5; i++)
		L[i] = PL[i];
}

Pyramide::Pyramide(Square PS[5])
{
	for(int i = 0; i < 5; i++)
			S[i] = PS[i];
}

void Line::Show()
{
	line(P1.GetX(), P1.GetY(), P2.GetX(), P2.GetY());
}

void Square::Show()
{
		for(int i = 0; i < 5; i++)
			L[i].Show();
}

void Pyramide::Show()
{
	for(int i = 0; i < 5; i++)
			S[i].Show();
}

int main()
{
	int gdriver = DETECT, gmode, errorcode;
	initgraph(&gdriver, &gmode, "");
	errorcode = graphresult();
	if(errorcode != grOk)
	{
		cout<<"Ошиба графики: "<<grapherrormsg(errorcode)<<endl;
		cout<<"Нажмите любую клавишу для прерывания: "<<endl;
		getch();	return (1);
	}
	setcolor(getmaxcolor());
	Point P1(100,250, 8);
	Point P2(200,250, 8);
	P1.Show();
	P2.Show();
	getch();
	Line L(P1,P2);
	L.Show();
	getch();
	Line L2[6];
	int DY = 0;
	for(int i = 0; i < 5; i++)
	{
		L2[i] = Line(Point(100,250 + DY ,15), Point(200,250 + DY,15));
		DY += 20;
	}
	Square S(L2);
	S.Show();
	Square S2[6];
	int start_X = 250;
	int finish_X = 450;
	int level = 350;
	DY = 0;
	int DX = 0;
	int Color[] = {15, 7, 10, 12, 5, 6};
	for(int k = 0; k < 5; k++)
	{
		for(int j = 0; j < 5 - k; j++)
		{
			L2[j] = Line(Point(start_X + DX, level + DY, Color[k]), Point(finish_X + DX ,level + DY,Color[k]));
			DX += 20;
			DY -= 35;
		}
		S2[k] = Square(L2);
		DX = 0;
		DY = 0;
		start_X += 15;
		finish_X -= 35;
		level -= 50;
	}
	getch();
	Pyramide R2(S2);
	R2.Show();
	getch();
	closegraph();
	return 0;
}
